﻿using System;

namespace exercise
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            Console.WriteLine("Hello, please type in a number");
            var num1 = int.Parse(Console.ReadLine());
            Console.WriteLine($"The number you typed in multiplied by 3 is {num1 * 3}");
            
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Do you wish to multiple again?");
            Console.ReadKey();
        }
    }
}
